#!/bin/bash
#author	Cosmin Planchon

if [ "$1" == "-h" ]; then
  echo "
Usage: `basename $0` extension string_to_find folder

Recursive search by default, ommitting the folder argument will search in current directory.

Case sensitive by default, to search case-insensitive strings add -c as last argument.
  
  exemple: `basename $0` .py PySide2 folder
"

  exit 0
fi
if [ "$1" == "" ]; then
  echo "
Please include an extension to research.

Usage: `basename $0` extension string_to_find folder
  
`basename $0` -h for more help
"
  exit 0
fi
if [ "$2" == "" ]; then
  echo "
Please include a string to research.

Usage: `basename $0` extension string_to_find folder
  
`basename $0` -h for more help
"
  exit 0
fi
if [ "$4" == "-c" ]; then
  grep -irlw --include="*$1" -e $2 $3 
  exit 0
fi
grep -rlw --include="*$1" -e $2 $3  

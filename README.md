# Search For String

## What is this ?
a bash script for Linux to locate strings in various file types
(just a grep command made easy)

## Usage

- Usage: searchStringinExt.sh extension string_to_find folder

Recursive search by default, ommitting the folder argument will search in current directory.

Case sensitive by default, to search case-insensitive strings add -c as last argument.
  
- exemple: searchStringinExt.sh .py PySide2 folder

## Installation

- Copy searchStringinExt.sh in your home folder

(make sure it is executable by running 'chmod +x searchStringinExt.sh')
